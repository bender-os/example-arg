SHELL := /bin/bash
NAME = test-arg-container
VERSION=0.1

default: build
.PHONY: build build_empty_arg

build:
	podman build --build-arg SOME_VARIABLE_NAME=a_value -t $(NAME):$(VERSION) .

build_empty_arg:
	podman build -t $(NAME):$(VERSION) .
