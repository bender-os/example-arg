FROM alpine:3.17.2

# ARG RELEASE_VERSION
# or with a hard-coded default:
ARG SOME_VARIABLE_NAME=default_value

RUN echo "My arg value is: $SOME_VARIABLE_NAME"
# RUN wget "http://nexus.dev.svc/repo/publi/SIMPLE-APP-${RELEASE_VERSION}.war" . 
# you could also use braces - ${SOME_VARIABLE_NAME}